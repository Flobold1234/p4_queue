#include <stdio.h>
#include <stdlib.h>

/*
 * @author: Florian Boldrick
 * In einer leeren Liste gibt es ein RootElement,
 * Damit muss das erste Element ignoriert werden
 *
 * Das letzte Element hat immer next = NULL
 */

typedef struct _Knoten_ {
    int value;
    struct _Knoten_ *next;
} Knoten;

Knoten * qinit() {
    Knoten *root = malloc(sizeof(root)); // Reservieren des Speichers für den Wurzelknoten
    root->next = NULL; // Next Element wird auf NULL gesetzt um end Element zu identifizieren
    root->value = 0; // Value auf 0, damit es im Debugger schoener aussieht
    return root;
}

int count(Knoten * root){
    if(root->next == NULL){
        return 0;
    } else {
        return 1 + count(root->next);
    }
}


int isEmpty(Knoten *root) {
    if(root->next == NULL) { // Wenn das Wurzelelement keinen Nachfolger hat ist die Liste leer
        return 1;
    }
    return 0;
}

void enqueue(Knoten *root, int element) {
    Knoten *new = malloc(sizeof(new));
    new->value = element;
    new->next = NULL; // Wichtig, damit auch beim Hinzufuegen eines 2. Elementes die nachfolgende Schleife funktioniert
    Knoten *dummy = root;
    while(dummy->next != NULL) {
        dummy = dummy->next; // Suchen nach dem letzten Element, also dem Element dessen Nachfolger NULL ist
    }
    dummy->next = new; // Der Nachfolger des alten letzten Elements wird auf den neuen Knoten gesetzt
}

int dequeue(Knoten *root) {
    if(isEmpty(root) == 0) { // Pruefen ob Liste Leer, ansonsten wird -INTMAX ausgegeben
        Knoten *dummy = root->next; // Durch die isEmpty Methode wird geprueft ob Root einen Nachfolger hat
        root->next = dummy->next; // Nachfolger von Root wird genommen und der Zeiger von Root auf den Nachfolger vom 1. echten Element gesetzt. Kein Problem wenn dessen Nachfolger NULL ist
        return dummy->value;
    } else {
        return -INT_MAX;
    }
}

void printQueue(Knoten *root) {
    if(isEmpty(root) == 0) {
        Knoten *dummy = root->next;
        int i = 1;
        while(dummy != NULL) {
            printf("%i: %i \n", i, dummy->value);
            dummy = dummy->next;
            i++;
        }
    } else {
        printf("Die Queue ist leer");
    }
}

int main() {
    int beendet = 0;
    int auswahl = 0;
    int element;

    Knoten *root = qinit();

    while (beendet == 0) {
        printf("\nMenue zur Warteschlangenverwaltung: \n"
               "1. Anfuegen eines Elementes \n"
               "2. Loeschen und Ausgeben des ersten Elementes \n"
               "3. Ausgeben der Warteschlange \n"
               "4. Beenden des Programms \n"
               "5. Groesse der Queue ausgeben\n"
               "Bitte geben Sie die Ziffer der gewunschten Funktion ein\n");
        scanf("%i", &auswahl);
        switch (auswahl) {
            case 1:
                element = rand() % 50;
                enqueue(root, element);
                break;
            case 2:
                element = dequeue(root);
                if(element == -INT_MAX) { // Wenn Queue leer ist wird ja -INT_MAX ausgegeben
                    printf("Die Queue ist leer und es kann kein Wert mehr ausgegeben werden");
                    break;
                }
                printf("%i wurde entfernt", element);
                break;
            case 3:
                printQueue(root);
                break;
            case 4:
                beendet = 1;
                break;
            case 5:
                printf("Die Groesse der Queue ist: %i",count(root));
                break;
            default:
                break;
        }
    }
    return 0;
}